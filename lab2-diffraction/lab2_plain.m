% Лабораторная работа 2
% Расчет картины дифракции плоской монохроматической электромагнитной волны на диэлектрическом цилиндре.
% Вариант 4. Рассчитать распределение интенсивности внутреннего и внешнего поля дифракции плоской ТЕ-поляризованной волны (длина волны λ = 633 нм) на бесконечном стеклянном цилиндре (n = 1,5) с радиусом R = λ (рис. 4.3). Рассчитать суммарную интенсивность для этого случая.

% Объявляем константы:
c = 2.99792458e14;              % скорость света в вакууме [мкм/с]
epsilon_0 = 8.8541878128e-12;   % электрическая постоянная [Ф/м]
mu_0 =  1.25663706212e-6;       % магнитная постоянная [Н/А^2]

% Объявляем входные данные:
lambda = 6.63e-7;     % длинна волны [м]
n = 1.5;         % показатель преломления цилиндра (стекло)

R = 1 * lambda; % радиус цилиндра [м]
epsilon1 = 1;   % диэлектрическая проницаемость среды
mu1 = 1;        % магнитная проницаемость среды
epsilon2 = 1;   % диэлектрическая проницаемость цилиндра
mu2 = 1;        % магнитная проницаемость цилиндра
A = 1;          % константа для начальной волны    
k1 = 2*pi/lambda;                  % волновое число
k2 = k1 * n;

N = 500;          % количество точек разбиения

% Инициализируем поля:
x = linspace(-4*R,4*R,N);
y = linspace(-4*R,4*R,N);
mspace = x;

z0 = [0,0,1];

% Находим коэффициенты b и c:
mspace_for_const =(-20:20);
b = nan([1,length(mspace_for_const)]);
c = nan([1,length(mspace_for_const)]);
n = 1;
for m = mspace_for_const
     b(n) = (k1*mu2*besselh(m, 2, k1*R)*besselj_div(m,k1*R)-k1*mu2*besselh_div(m, 2, k1*R)*besselj(m,k1*R)) / ...
     (k2*mu1*besselj_div(m,k2*R)*besselh(m, 2, k1*R)-k1*mu2*besselh_div(m, 2, k1*R)*besselj(m,k2*R));
     c(n) = (k2*mu1*besselj_div(m,k2*R)*besselj(m,k1*R) - k1*mu2*besselj(m,k2*R)*besselj_div(m,k1*R)) / ...
     (k1*mu2*besselj(m,k2*R)*besselh_div(m, 2, k1*R) - k2*mu1*besselj_div(m,k2*R)*besselh(m, 2, k1*R));
     n = n + 1;
end

plot(mspace_for_const,abs(b))
xlabel("m")
ylabel("abs(b)")
plot(mspace_for_const, abs(c))
xlabel("m")
ylabel("abs(c)")

% Находим E+ и E-:


E_plus_result= zeros(N, N);
E_minus_result = zeros(N, N);
E_result = zeros(N, N);

for ii = 1:N
    for jj = 1:N
        
        % переход к полярным координатам
        [theta, rho] = cart2pol(x(ii),y(jj));
        
        % Интенсивность
        E_plus = z0 * A * sum(b .* (-1i).^mspace_for_const .* exp(1i * mspace_for_const * theta) .* besselj(mspace_for_const,k2 * rho));
        E_minus = z0 * A * sum(c .* (-1i).^mspace_for_const .* exp(1i * mspace_for_const * theta) .* besselh(mspace_for_const, 2, k1 * rho));
        E_0 = z0 * A * exp(-1i * k1 * rho * cos(theta));
        
        I_plus = norm(E_plus);
        I_minus = norm(E_minus);
        I_result = norm(E_0 + E_minus);
        if rho < R
            E_plus_result(ii, jj) = I_plus;
            E_result(ii, jj) = I_plus;
        else
            E_minus_result(ii, jj) = I_minus;
            E_result(ii, jj) = I_result;
        end
        
    
    end
end

% Строим графики:
figure;
imagesc(x, flipud(y), E_plus_result);
title('Внутреннее поле дифракции')
xlabel('x, м');
ylabel('y, м');
colorbar;
ax = gca;
ax.YDir = 'normal';

figure;
imagesc(x, y, E_minus_result);
title('Внешнее поле дифракции')
xlabel('x, м');
ylabel('y, м');
colorbar;
ax = gca;
ax.YDir = 'normal';

figure;
imagesc(x, y, E_result);
title('Результирующее поле дифракции')
xlabel('x, м');
ylabel('y, м');
colorbar;
ax = gca;
ax.YDir = 'normal';

% Функции:
function bess = besselj_div(nu, Z)
    bess = besselj(nu-1,Z) - (nu/Z) * besselj(nu,Z); 
end
function bess = besselh_div(nu, K, Z)
    bess = besselh(nu-1, K, Z) - (nu/Z) * besselh(nu, K, Z); 
end
