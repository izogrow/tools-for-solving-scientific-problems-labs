% Лабораторная работа 3
% Расчет распределения интенсивности светового поля в фокусе широкоапертурной линзы с использованием формул Ричардса-Вольфа.
% Вариант 4. Получить поперечные распределения интенсивности светового поля в фокусе апланатического объектива при фокусировке радиально-поляризованной моды R-TEM01 (параметр β считать равным 1) с длиной волны 0,532 мкм в воздухе (n = 1). Вычисления произвести для значений числовой апертуры NA равной 0,65, 0,8 и 0,95. Повторить вычисления для параметра β равного 0,5, 1,5, 2,0.
% 
% Объявляем входные данные:
lambda = 5.32e-7;     % длинна волны [м]
n = 1;         % показатель преломления среды
k = 2 * pi / lambda;               % волновое число

betta = 2;
z = 0;         % в фокусе
A = 1;          % константа
A_l = 1;        % константа
NA = [0.65, 0.8, 0.95];

a = 0.000001;          % размер поля 
N = 501;          % количество точек разбиения

% Инициализируем поля:
r = linspace(-a, a, N);


% Находим E_r, E_z и интенсивность:
T = @(theta) cos(theta).^(1/2);

I = nan([length(r), length(NA)]);
ii = 1;
for NA_i = NA
    l = @(theta) A_l.* sin(theta).* exp(-(betta.^2*sin(theta).^2*n.^2)./ NA_i.^2);
    Er_inner = @(theta) l(theta).* T(theta).* sin(2*theta).* exp(1i.* k.* z.* cos(theta)).* besselj(1, k* r.* sin(theta));
    Ez_inner = @(theta) l(theta).* T(theta).* sin(theta)^2.* exp(1i.* k.* z.* cos(theta)) * besselj(0, k * r.* sin(theta));
    
    Er = A * integral(Er_inner, 0, asin(NA_i), 'ArrayValued',true);
    Ez = 2 * 1i * A * integral(Ez_inner, 0, asin(NA_i), 'ArrayValued',true);
    
    I(:,ii) = abs(Er).^2 + abs(Ez).^2;
    ii = ii + 1;
end

% Стрим графики:
plot(r, I(:,1)./max(I(:,1))) % строим в относительных координатах y
title(sprintf("NA = %f", NA(1)))
xlabel("r, m")
ylabel("Intensity, a. u.")

plot(r, I(:,2)./max(I(:,2)))
title(sprintf("NA = %f", NA(2)))
xlabel("r, m")
ylabel("Intensity, a. u.")

plot(r, I(:,3)./max(I(:,3)))
title(sprintf("NA = %f", NA(3)))
xlabel("r, m")
ylabel("Intensity, a. u.")
